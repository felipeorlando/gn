const config = require('./config.js');

module.exports = {
  bail: true,
  entry: config.js.entry,
  output: {
    filename: config.js.output.file
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
      },
    ]
  }
};
