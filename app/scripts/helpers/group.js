const obj      = require("./object.js");
const dom      = require("./dom.js");
const fieldset = require("./fieldset.js");

module.exports = {
  generateGroups(object) {
    let tabs = object._embedded;

    obj.each(tabs, (groups) => {
      let tab = dom.create("div", {class: "tab__content"});

      obj.each(tabs[groups], (input) => {
        tab.appendChild(this.createField(tabs[groups][input]));
      });

      tab.appendChild(fieldset.createButton());
      this.html.appendChild(tab);
    });

    dom.pushToForm(this.html);
  },
};
