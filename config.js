module.exports = {
  js: {
    files:  "./app/scripts/**/*.js",
    entry: "./app/scripts/main.js",
    output: {
      file: "bundle.js",
      dir:  "./public/scripts"
    }
  },
  css: {
    files:  "./app/styles/**/*.scss",
    entry:  "./app/styles/main.scss",
    unmins: "./app/unminified-style/**/*.css",
    output: {
      file:  "main.css",
      dir:   "./public/styles",
      unmin: "./app/unminified-style/"
    }
  },
  img: {
    entry:  "./app/images/**/*",
    output: "./public/images/"
  },
  mainFile: "./public/"
}
