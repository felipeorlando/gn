module.exports = {
  each(object, callback) {
    Object.keys(object).forEach((key) => {
      callback(key);
    });
  },

  eachWithIndex(object, callback) {
    let count = 0;
    Object.keys(object).forEach((key) => {
      callback(key, count);
      count++;
    });
  },
};
