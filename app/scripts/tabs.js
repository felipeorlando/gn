module.exports = {
  changeTab() {
    this.content.change();
    this.button.change();
    document.body.scrollTop = 300;
  },

  content: {
    change() {
      this.disabled().className += ` ${this.enabledClass}`;
      this.enabled().classList.remove(this.enabledClass);
    },

    enabled() {
      return document.querySelector(`.${this.enabledClass}`);
    },

    disabled() {
      return document.querySelector(`.${this.disabledClass}`);
    },

    enabledClass:   "tab__content--active",
    disabledClass:  "tab__content:not(.tab__content--active)",
  },

  button: {
    change() {
      this.disabled().className += ` ${this.enabledClass}`;
      this.enabled().classList.remove(this.enabledClass);
    },

    enabled() {
      return document.querySelector(`.${this.enabledClass}`);
    },

    disabled() {
      return document.querySelector(`.${this.disabledClass}`);
    },

    enabledClass:   "tab__button--active",
    disabledClass:  "tab__button:not(.tab__button--active)",
  },
}
