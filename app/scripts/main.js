const tabs       = require("./tabs.js");
const validation = require("./validation.js");
const form       = require("./form.js")

const header  = document.querySelector(".header");
const footer  = document.querySelector(".footer");

document.addEventListener('DOMContentLoaded', () => {
  form.generateForm();

  setTimeout(() => {
    const submits = document.querySelectorAll(".form .button");
    submits.forEach((button) => {
      button.addEventListener("click", (e) => {
        validation.validate(e, tabs.changeTab.bind(tabs));
      });
    });
  }, 1000);
}, false);
