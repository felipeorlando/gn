const fieldset   = require("./fieldset.js");
const collection = require("./collection.js");

module.exports = {
  small_text(options) { return fieldset.createInput(options); },
  email(options)      { return fieldset.createInput(options); },
  phone(options)      { return fieldset.createInput(options); },
  lat_lng(options)    { return fieldset.createInput(options); },
  big_text(options)   { return fieldset.createText(options);  },
  enumerable(options) {
    if (options.allow_multiple_value) return collection.checkboxes(options);
    return collection.createSelect(options);
  },
};
