module.exports = {
  getJSON(url, callback) {
    let request = new XMLHttpRequest();

    request.onreadystatechange = function() {
      if (request.readyState === 4) {
        if (request.status === 200) {
          callback(JSON.parse(this.responseText));
        } else {
          console.log(request);
          alert("Houston, we have a problem!");
        }
      }
    };
    request.open("GET", url);
    request.send();
  },
};
