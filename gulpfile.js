// gulp and gulp-deps
const gulp          = require("gulp");
const webpack       = require("webpack");
const webpackStream = require("webpack-stream");
const sass          = require("gulp-sass");
const cleanCSS      = require("gulp-clean-css");
const imagemin      = require("gulp-imagemin");
const notifier      = require("node-notifier");
const livereload    = require("gulp-livereload");
const browserSync   = require("browser-sync").create();

// config files
const config        = require("./config.js");
const webpackConfig = require("./webpack.config.js");

// webpack -> es6 to es5
gulp.task("webpack", () => {
  return gulp.src(config.js.entry)
    .pipe(webpackStream(webpackConfig))
    .pipe(gulp.dest(config.js.output.dir));
});

// sass to css
gulp.task("sass", () => {
  return gulp.src(config.css.entry)
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest(config.css.output.unmin));
});

// css to monified css
gulp.task("minify-css", () => {
  return gulp.src(config.css.unmins)
    .pipe(cleanCSS({compatibility: "ie8"}))
    .pipe(gulp.dest(config.css.output.dir));
});

// compress images
gulp.task("images", () => {
  return gulp.src(config.img.entry)
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}]
    }))
    .pipe(gulp.dest(config.img.output));
});

// watch diffs
gulp.task("watch", () => {
  livereload.listen();
  gulp.watch(config.js.files,   ["webpack"]);
  gulp.watch(config.css.files,  ["sass"]);
  gulp.watch(config.css.unmins, ["minify-css"]);
  gulp.watch(config.img.entry,  ["images"]);
});

// server with browserSync
gulp.task("serve", ["minify-css", "webpack"], () => {
  browserSync.init({
    server: {
      baseDir: config.mainFile
    }
  });

  gulp.watch("./public/**/*").on("change", browserSync.reload);
});

// default task
gulp.task("default", [
  "webpack", "sass", "minify-css", "images", "serve", "watch"
]);
