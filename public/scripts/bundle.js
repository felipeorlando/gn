/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	const tabs = __webpack_require__(1);
	const validation = __webpack_require__(2);
	const form = __webpack_require__(5);

	const header = document.querySelector(".header");
	const footer = document.querySelector(".footer");

	document.addEventListener('DOMContentLoaded', () => {
	  form.generateForm();

	  setTimeout(() => {
	    const submits = document.querySelectorAll(".form .button");
	    submits.forEach(button => {
	      button.addEventListener("click", e => {
	        validation.validate(e, tabs.changeTab.bind(tabs));
	      });
	    });
	  }, 1000);
	}, false);

/***/ }),
/* 1 */
/***/ (function(module, exports) {

	module.exports = {
	  changeTab() {
	    this.content.change();
	    this.button.change();
	    document.body.scrollTop = 300;
	  },

	  content: {
	    change() {
	      this.disabled().className += ` ${this.enabledClass}`;
	      this.enabled().classList.remove(this.enabledClass);
	    },

	    enabled() {
	      return document.querySelector(`.${this.enabledClass}`);
	    },

	    disabled() {
	      return document.querySelector(`.${this.disabledClass}`);
	    },

	    enabledClass: "tab__content--active",
	    disabledClass: "tab__content:not(.tab__content--active)"
	  },

	  button: {
	    change() {
	      this.disabled().className += ` ${this.enabledClass}`;
	      this.enabled().classList.remove(this.enabledClass);
	    },

	    enabled() {
	      return document.querySelector(`.${this.enabledClass}`);
	    },

	    disabled() {
	      return document.querySelector(`.${this.disabledClass}`);
	    },

	    enabledClass: "tab__button--active",
	    disabledClass: "tab__button:not(.tab__button--active)"
	  }
	};

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

	const dom = __webpack_require__(3);

	module.exports = {
	  validate(e, callback) {
	    e.preventDefault();
	    const content = dom.parentClass(e.srcElement, this.content);
	    const inputs = this.requiredInputs(content, `.${this.requiredInput}`);
	    const checks = this.requiredInputs(content, `.${this.requiredCheck}`);
	    let errors = false;

	    if (inputs.length && this.validateInputs(inputs)) errors = true;
	    if (checks.length && this.validateInputs(checks, true)) errors = true;

	    if (!errors) callback();

	    return false;
	  },

	  validateInputs(inputs, isCheckbox = false, errors = false) {
	    inputs.forEach(input => {
	      if (this.hasErrors(input, isCheckbox)) {
	        console.log('has errors');
	        errors = true;
	        this.markError(input);
	      } else {
	        console.log('no errors');
	        this.removeError(input);
	      }
	    });

	    return errors;
	  },

	  requiredInputs(group, toQuery) {
	    return group.querySelectorAll(toQuery);
	  },

	  hasErrors(input, isCheckbox) {
	    if (isCheckbox) return !input.querySelectorAll(this.checked).length;
	    if (input.type == 'select-one') return !input.value;
	    return !input.value.length;
	  },

	  markError(input) {
	    let message = dom.brother(input, `.${this.message}`);
	    message.className += ` ${this.messageError}`;

	    if (input.classList.contains(this.input)) {
	      input.className += ` ${this.inputError}`;
	    }
	  },

	  removeError(input) {
	    let message = dom.brother(input, `.${this.message}`);
	    message.classList.remove(this.messageError);

	    if (input.classList.contains(this.input)) {
	      input.classList.remove(this.inputError);
	    }
	  },

	  groupInputs(element, content) {
	    return dom.parentClass(element, content);
	  },

	  content: "tab__content",
	  checked: "input:checked",
	  requiredInput: "form__input--required",
	  requiredCheck: "form__checks--required",
	  input: "form__input",
	  inputError: "form__input--error",
	  message: "form__message",
	  messageError: "form__message--error"
	};

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

	const obj = __webpack_require__(4);

	module.exports = {
	  create(name, attrs = {}) {
	    let element = document.createElement(String(name));

	    for (let attr in attrs) {
	      element.setAttribute(attr, attrs[attr]);
	    }

	    return element;
	  },

	  pushToForm(html) {
	    document.querySelector(".form").appendChild(html);
	    document.querySelector(".tab__content").className += " tab__content--active";
	  },

	  parentClass(el, cls) {
	    while ((el = el.parentElement) && !el.classList.contains(cls));
	    return el;
	  },

	  brother(el, query) {
	    return el.parentElement.querySelector(query);
	  }
	};

/***/ }),
/* 4 */
/***/ (function(module, exports) {

	module.exports = {
	  each(object, callback) {
	    Object.keys(object).forEach(key => {
	      callback(key);
	    });
	  },

	  eachWithIndex(object, callback) {
	    let count = 0;
	    Object.keys(object).forEach(key => {
	      callback(key, count);
	      count++;
	    });
	  }
	};

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

	const xhr = __webpack_require__(6);
	const obj = __webpack_require__(4);
	const dom = __webpack_require__(3);
	const group = __webpack_require__(7);
	const fieldset = __webpack_require__(8);
	const actions = __webpack_require__(9);

	module.exports = {
	  generateForm() {
	    this.makeRequest(group.generateGroups.bind(this));
	  },

	  createField(options) {
	    let row = dom.create("div", { class: "row" });
	    let group = dom.create("div", { class: "form__group" });
	    let label = fieldset.createLabel(options);

	    group.appendChild(label);

	    let field = actions[options.type](options);

	    group.appendChild(field);
	    if (options.required) group.appendChild(fieldset.errorMessage());
	    row.appendChild(group);
	    return row;
	  },

	  makeRequest(callback) {
	    xhr.getJSON("./json/fields.json", callback);
	  },

	  html: dom.create("div")
	};

/***/ }),
/* 6 */
/***/ (function(module, exports) {

	module.exports = {
	  getJSON(url, callback) {
	    let request = new XMLHttpRequest();

	    request.onreadystatechange = function () {
	      if (request.readyState === 4) {
	        if (request.status === 200) {
	          callback(JSON.parse(this.responseText));
	        } else {
	          alert("Houston, we have a problem!");
	        }
	      }
	    };
	    request.open("GET", url);
	    request.send();
	  }
	};

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

	const obj = __webpack_require__(4);
	const dom = __webpack_require__(3);
	const fieldset = __webpack_require__(8);

	module.exports = {
	  generateGroups(object) {
	    let tabs = object._embedded;

	    obj.each(tabs, groups => {
	      let tab = dom.create("div", { class: "tab__content" });

	      obj.each(tabs[groups], input => {
	        tab.appendChild(this.createField(tabs[groups][input]));
	      });

	      tab.appendChild(fieldset.createButton());
	      this.html.appendChild(tab);
	    });

	    dom.pushToForm(this.html);
	  }
	};

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

	const dom = __webpack_require__(3);

	module.exports = {
	  createInput(options, big = false) {
	    let input = dom.create(big ? "textarea" : "input", {
	      class: "form__input",
	      name: options.name,
	      for: options.label,
	      placeholder: options.placeholder
	    });
	    if (options.required) input.className += " form__input--required";
	    return input;
	  },

	  createText(options) {
	    return this.createInput(options, true);
	  },

	  createLabel(options, index = false, value = false) {
	    let label = dom.create("label", {
	      class: "form__label",
	      for: index !== false ? `${options.name}-${index}` : options.label
	    });

	    label.innerHTML = value || options.label;

	    return label;
	  },

	  createButton() {
	    let button = dom.create("button", {
	      type: "button",
	      class: "button button--primary button--fluid"
	    });
	    button.innerHTML = "Prosseguir";
	    return button;
	  },

	  errorMessage() {
	    let message = dom.create("span", { class: "form__message" });
	    message.innerHTML = "Este campo é obrigatório.";
	    return message;
	  }
	};

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

	const fieldset = __webpack_require__(8);
	const collection = __webpack_require__(10);

	module.exports = {
	  small_text(options) {
	    return fieldset.createInput(options);
	  },
	  email(options) {
	    return fieldset.createInput(options);
	  },
	  phone(options) {
	    return fieldset.createInput(options);
	  },
	  lat_lng(options) {
	    return fieldset.createInput(options);
	  },
	  big_text(options) {
	    return fieldset.createText(options);
	  },
	  enumerable(options) {
	    if (options.allow_multiple_value) return collection.checkboxes(options);
	    return collection.createSelect(options);
	  }
	};

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

	const obj = __webpack_require__(4);
	const dom = __webpack_require__(3);
	const fieldset = __webpack_require__(8);

	module.exports = {
	  checkboxes(options) {
	    let ul = dom.create("ul", { id: options.name });
	    if (options.required) ul.className = "form__checks--required";

	    obj.eachWithIndex(options.values, (value, index) => {
	      let li = dom.create("li", { class: "form__check" });
	      let label = fieldset.createLabel(options, index, value);
	      let input = this.createCheckbox(options, value, index);

	      li.appendChild(input);
	      li.appendChild(label);

	      ul.appendChild(li);
	    });

	    return ul;
	  },

	  createCheckbox(options, value, index) {
	    return dom.create("input", {
	      type: "checkbox",
	      id: `${options.name}-${index}`,
	      class: "form__check__box",
	      name: options.name,
	      value: value
	    });
	  },

	  createSelect(options) {
	    let selectInput = dom.create("select", {
	      class: "form__input",
	      name: options.name,
	      placeholder: options.placeholder
	    });

	    if (options.required) selectInput.className += " form__input--required";

	    if (options.placeholder) {
	      let selectedOption = this.createOption(options.placeholder, true);
	      selectInput.appendChild(selectedOption);
	    }

	    obj.eachWithIndex(options.values, (value, index) => {
	      let option = this.createOption(value);
	      selectInput.appendChild(option);
	    });

	    return selectInput;
	  },

	  createOption(value, placeholder = false) {
	    let inputOption = dom.create("option", { value: value });
	    if (placeholder) {
	      inputOption.disabled = true;
	      inputOption.selected = true;
	      inputOption.value = "";
	    }
	    inputOption.innerHTML = value;
	    return inputOption;
	  }
	};

/***/ })
/******/ ]);