const obj = require("./object.js");

module.exports = {
  create(name, attrs = {}) {
    let element = document.createElement(String(name));

  	for (let attr in attrs) {
  		element.setAttribute(attr, attrs[attr]);
  	}

  	return element;
  },

  pushToForm(html) {
    document.querySelector(".form").appendChild(html);
    document.querySelector(".tab__content").className += " tab__content--active";
  },

  parentClass(el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls));
    return el;
  },

  brother(el, query) {
    return el.parentElement.querySelector(query);
  }
};
