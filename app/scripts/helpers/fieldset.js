const dom = require("./dom.js");

module.exports = {
  createInput(options, big = false) {
    let input = dom.create(big ? "textarea" : "input", {
      class: "form__input",
      name: options.name,
      for: options.label,
      placeholder: options.placeholder
    });
    if (options.required) input.className += " form__input--required";
    return input;
  },

  createText(options) {
    return this.createInput(options, true);
  },

  createLabel(options, index = false, value = false) {
    let label = dom.create("label", {
      class: "form__label",
      for: index !== false ? `${options.name}-${index}` : options.label
    });

    label.innerHTML = value || options.label;

    return label;
  },

  createButton() {
    let button = dom.create("button", {
      type: "button",
      class: "button button--primary button--fluid"
    });
    button.innerHTML = "Prosseguir";
    return button;
  },

  errorMessage() {
    let message = dom.create("span", {class: "form__message"});
    message.innerHTML = "Este campo é obrigatório.";
    return message;
  },
};
