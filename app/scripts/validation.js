const dom = require("./helpers/dom.js");

module.exports = {
  validate(e, callback) {
    e.preventDefault();
    const content = dom.parentClass(e.srcElement, this.content);
    const inputs  = this.requiredInputs(content, `.${this.requiredInput}`);
    const checks  = this.requiredInputs(content, `.${this.requiredCheck}`);
    let errors = false;

    if (inputs.length && this.validateInputs(inputs)) errors = true;
    if (checks.length && this.validateInputs(checks, true)) errors = true;

    if (!errors) callback();

    return false;
  },

  validateInputs(inputs, isCheckbox = false, errors = false) {
    inputs.forEach((input) => {
      if (this.hasErrors(input, isCheckbox)) {
        errors = true;
        this.markError(input);
      } else {
        this.removeError(input);
      }
    });

    return errors;
  },

  requiredInputs(group, toQuery) {
    return group.querySelectorAll(toQuery);
  },

  hasErrors(input, isCheckbox) {
    if (isCheckbox) return !input.querySelectorAll(this.checked).length;
    if (input.type == 'select-one') return !input.value;
    return !input.value.length;
  },

  markError(input) {
    let message = dom.brother(input, `.${this.message}`);
    message.className += ` ${this.messageError}`;

    if (input.classList.contains(this.input)) {
      input.className += ` ${this.inputError}`;
    }
  },

  removeError(input) {
    let message = dom.brother(input, `.${this.message}`);
    message.classList.remove(this.messageError);

    if (input.classList.contains(this.input)) {
      input.classList.remove(this.inputError);
    }
  },

  groupInputs(element, content) {
    return dom.parentClass(element, content);
  },

  content:        "tab__content",
  checked:        "input:checked",
  requiredInput:  "form__input--required",
  requiredCheck:  "form__checks--required",
  input:          "form__input",
  inputError:     "form__input--error",
  message:        "form__message",
  messageError:   "form__message--error"
};
