const obj      = require("./object.js");
const dom      = require("./dom.js");
const fieldset = require("./fieldset.js");

module.exports = {
  checkboxes(options) {
    let ul = dom.create("ul", {id: options.name});
    if (options.required) ul.className = "form__checks--required";

    obj.eachWithIndex(options.values, (value, index) => {
      let li    = dom.create("li", {class: "form__check"});
      let label = fieldset.createLabel(options, index, value);
      let input = this.createCheckbox(options, value, index);

      li.appendChild(input);
      li.appendChild(label);

      ul.appendChild(li);
    });

    return ul;
  },

  createCheckbox(options, value, index) {
    return dom.create("input", {
      type: "checkbox",
      id: `${options.name}-${index}`,
      class: "form__check__box",
      name: options.name,
      value: value
    });
  },

  createSelect(options) {
    let selectInput = dom.create("select", {
      class: "form__input",
      name: options.name,
      placeholder: options.placeholder
    });

    if (options.required) selectInput.className += " form__input--required";

    if (options.placeholder) {
      let selectedOption = this.createOption(options.placeholder, true);
      selectInput.appendChild(selectedOption);
    }

    obj.eachWithIndex(options.values, (value, index) => {
      let option = this.createOption(value);
      selectInput.appendChild(option);
    });

    return selectInput;
  },

  createOption(value, placeholder = false) {
    let inputOption = dom.create("option", {value: value});
    if (placeholder) {
      inputOption.disabled = true;
      inputOption.selected = true;
      inputOption.value    = "";
    }
    inputOption.innerHTML = value;
    return inputOption;
  },
};
