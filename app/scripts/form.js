const xhr        = require("./helpers/xhr.js");
const obj        = require("./helpers/object.js");
const dom        = require("./helpers/dom.js");
const group      = require("./helpers/group.js");
const fieldset   = require("./helpers/fieldset.js");
const actions    = require("./helpers/actions.js");

module.exports = {
  generateForm() {
    this.makeRequest(group.generateGroups.bind(this));
  },

  createField(options) {
    let row   = dom.create("div", {class: "row"});
    let group = dom.create("div", {class: "form__group"});
    let label = fieldset.createLabel(options);

    group.appendChild(label);

    let field = actions[options.type](options);

    group.appendChild(field);
    if (options.required) group.appendChild(fieldset.errorMessage());
    row.appendChild(group);
    return row;
  },

  makeRequest(callback) {
    xhr.getJSON("./json/fields.json", callback);
  },

  html: dom.create("div"),
};
